package acestream

import (
	"github.com/valyala/fasthttp"
	"github.com/spf13/viper"
)



// API Method
// Terms: <engine_address> - app engine IP address, <engine_port> - app engine HTTP port. Common params:
// - id: content id (conditional param)
// - infohash: transport file infohash (.acelive or .torrent file) (conditional param)
// - url: link to transport file (conditional param). Can be used with URL encoded "file://path/" to access local file.
// - pid: player id (optional, formerly known as sid, since ver. 3.1.29 is obsolete and replaced by pid)

var aceEngine string

func init() {
	aceEngine = viper.GetString("ace.engine.server")
}

// GetVersion func
func GetVersion() Version {
	res, err := request("GET", apiService, Request{Method: "get_version", Format: "json"})

	if err != nil {

	}

	type resStruct struct {
		Result Version `json:"result"`
		Err error `json:"error,omitempty"`
	}

	resVersion := res.(resStruct)

	return resVersion.Result
}

// GetHTTPstream func
func GetHTTPstream(id string, pid string) {

}

// GetHLSstream func
func GetHLSstream(id string, pid string) {

}

// StopStream func
func Stopstream(url string) {

}

func request(method string, path string, params Request) interface{}, error {
	data, _ := json.Marshal(message)

	requestUrl := fmt.Sprintf("%s%s", aceEngine, path)

	req := fasthttp.AcquireRequest()
	req.SetRequestURI(requestUrl)
	req.Header.Add("Content-Type", "application/json")
	req.Header.SetMethod(method)
	req.SetBody(data)

	resp := fasthttp.AcquireResponse()
	client := &fasthttp.Client{}
	err := client.Do(req, resp)

	bodyBytes := resp.Body()
	println(string(bodyBytes))


	return err
}
