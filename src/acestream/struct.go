package acestream

// Version struct
type Version struct {
	Code    int64  `json:"code"`
	Version string `json:"engine"`
}

// StreamResponse struct
type StreamResponse struct {
	Response AceDetail `json:"response"`
	Error    error     `json:"error"`
}

// AceDetail struct
type AceDetail struct {
	Encrypted int32  `json:"is_encrypted"`
	Session   string `json:"playback_session_id"`
	Live      int32  `json:"is_live"`
	Command   string `json:"command_url"`
	Event     string `json:"event_url"`
	Playback  string `json:"playback_url"`
	Stat      string `json:"stat_url"`
}

type Request struct {
	Method                 string `json:"method,omitempty"`                   // method=get_version
	ID                     string `json:"id,omitempty"`                       // content id
	PID                    string `json:"pid,omitempty"`                      // pid - player id (optional, formerly known as sid, since ver. 3.1.29 is obsolete and replaced by pid)
	TranscodeAudio         int    `json:"transcode_audio,omitempty"`          // transcode all audio tracks to AAC, (values: 0 or 1, default 0)
	TranscodeMp3           int    `json:"transcode_mp3,omitempty"`            // do not transcode MP3 track(s), (values: 0 or 1, default 0)
	TranscodeAc3           int    `json:"transcode_ac3,omitempty"`            // transcode only AC3 track(s), (values: 0 or 1, default 0)
	PreferredAudioLanguage string `json:"preferred_audio_language,omitempty"` // three char code of preffered language, full list - http://xml.coverpages.org/nisoLang3-1994.html
	Format                 string `json:"format,omitempty"`                   // json, jsonp
	Callback               string `json:"callback,omitempty"`                 // Jsonp callback name
}

type StatResponse struct {
	Response StatDetail `json:"response"`
	Error    error      `json:"error,omitempty"`
}

type StatDetail struct {
	Status        string `json:"status"`
	Uploaded      int64  `json:"uploaded"`
	SpeedDown     int64  `json:"speed_down"`
	SpeedUp       int64  `json:"speed_up"`
	Downloaded    int64  `json:"downloaded"`
	Peers         int32  `json:"peers"`
	TotalProgress int32  `json:"total_progress"`
}
